/* COMP2215 Task 5---SKELETON */

#include "os.h"

int blink(int);
int update_dial(int);
int collect_delta(int);
int check_switches(int);
void tail(char*);


FIL File;                   /* FAT File */

int position = 0;



void main(void) {
    os_init();

    os_add_task( blink,            30, 1);
    os_add_task( collect_delta,   500, 1);
    os_add_task( check_switches,  100, 1);

    sei();
    for(;;){}

}


int collect_delta(int state) {
	position += os_enc_delta();
	return state;
}


int check_switches(int state) {

	if (get_switch_press(_BV(SWN))) {
			display_string("North\n");
	}

	if (get_switch_press(_BV(SWE))) {
			display_string("East\n");
	}

	if (get_switch_press(_BV(SWS))) {
			display_string("South\n");
	}

	if (get_switch_press(_BV(SWW))) {
    tail("myfile.txt");
	}

	if (get_switch_long(_BV(SWC))) {
		f_mount(&FatFs, "", 0);
		if (f_open(&File, "myfile.txt", FA_WRITE | FA_OPEN_ALWAYS) == FR_OK) {
			f_lseek(&File, f_size(&File));
			f_printf(&File, "Encoder position is: %d \r\n", position);
			f_close(&File);
			display_string("Wrote position\n");
		} else {
			display_string("Can't write file! \n");
		}

	}

	if (get_switch_short(_BV(SWC))) {
			display_string("[S] Centre\n");
	}

	if (get_switch_rpt(_BV(SWN))) {
			display_string("[R] North\n");
	}

	if (get_switch_rpt(_BV(SWE))) {
			display_string("[R] East\n");
	}

	if (get_switch_rpt(_BV(SWS))) {
			display_string("[R] South\n");
	}

	if (get_switch_rpt(SWN)) {
			display_string("[R] North\n");
	}

	if (get_switch_long(_BV(OS_CD))) {
		display_string("Detected SD card.\n");
	}

	return state;
}




int blink(int state) {
	static int light = 0;
	uint8_t level;

	if (light < -120) {
		state = 1;
	} else if (light > 254) {
		state = -20;
	}


	/* Compensate somewhat for nonlinear LED
           output and eye sensitivity:
        */
	if (state > 0) {
		if (light > 40) {
			state = 2;
		}
		if (light > 100) {
			state = 5;
		}
	} else {
		if (light < 180) {
			state = -10;
		}
		if (light < 30) {
			state = -5;
		}
	}
	light += state;

	if (light < 0) {
		level = 0;
	} else if (light > 255) {
		level = 255;
	} else {
		level = light;
	}

	os_led_brightness(level);
	return state;
}

void tail(char* filename) {
  clear_screen();
  char message[20];
  sprintf(message, "Tail of %s\r\n", filename);
  display_string(message);
  f_mount(&FatFs, "", 0);
  if (f_open(&File, filename, FA_READ | FA_OPEN_EXISTING) == FR_OK) {
    // Go to end of file
    f_lseek(&File, f_size(&File));
    // Seek backwards until beginning of file or 25 newlines seen
    int newlines = 0;
    while (newlines < 25 && f_tell(&File) > 0) {
      UINT read;
      char data[1];
      f_read(&File, data, 1, &read);
      if (*data == '\n') newlines ++;
      f_lseek(&File, f_tell(&File) - 2);
    }
    char data[255];
    while(f_gets(data,255,&File)) {
      display_string(data);
    }
  }
}
